<?php
require_once(__DIR__ . '/Console.php');
require_once(__DIR__ . '/Computer.php');
require_once(__DIR__ . '/Asus.php');
require_once(__DIR__ . '/Lenovo.php');
require_once(__DIR__ . '/Macbook.php');

$computer = new Asus();
if ($computer instanceof Asus) {
	$computer->start();
	sleep(1);
	$computer->restart2();
	sleep(1);
	$computer->shutDown();
	sleep(1);
	$computer->restart2();
}